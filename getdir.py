# -*- coding: utf-8 -*-
import os


def dimana():
    """Modul untuk mendapatkan direktori aktif aplikasi berada
    beberapa keterangan
    os.path.dirname(file), itu untuk menampilkan direktori dia berada
    sedangkan __file__ adalah kembalian dari nama file kita yang sedang
    berjalan"""
    home = os.path.dirname(__file__)
    return home

