#!/usr/bin/python
from gi.repository import Gtk
from moduls.tpressed import Targeem
from moduls.searchenter import SEnter
from about.aboutdlg import AboutDlg
from listfile import ListKamus
import getdir

path = getdir.dimana()
class MainGui:

    def __init__(self):
        self.jendela = Gtk.Window(title='Targeem v 1.0', border_width=5,
            window_position=3)
        self.jendela.connect('destroy', Gtk.main_quit)
        self.jendela.set_icon_from_file(path+"/images/book24.png")

        #layout table
        tabel_layout = Gtk.Table(9,7, True)
        self.jendela.add(tabel_layout)
        combobox = Gtk.ComboBoxText()
        combobox_pilih_bahasa = ['ArabIndo','IndoArab']
        combobox.set_entry_text_column(0)

        for x in combobox_pilih_bahasa:
            combobox.append_text(x)
        #search entry
        search_entry = Gtk.Entry()
        #About tombol
        #about = Gtk.Button('_About', use_underline=True)


        #about.connect('clicked', AboutDlg)
        #targeem tombol
        targeem_button = Gtk.Button('_Targeem', use_underline=True,
            )
        #targeem_button



        targeem_pressed = Targeem()
        search_enter = SEnter()
        #combobox


        #make vbox
        #vbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)



        frame_dari_ke = Gtk.Frame(label="DariKe")
        frame_dari_ke.set_shadow_type(4)
        tabel_layout.attach(frame_dari_ke, 1, 2, 7, 9)
        frame_dari_ke.add(combobox)
        frame_pencarian = Gtk.Frame(label="Pencarian")
        tabel_layout.attach(frame_pencarian, 2,6,7,9)
        vbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        search_entry.set_width_chars(40)
        vbox.add(search_entry)
        vbox.add(targeem_button)
        tabel_layout.set_col_spacings(5)
        frame_pencarian.add(vbox)
        #buat scrolled window
        scrolled_window = Gtk.ScrolledWindow(hexpand=True)
        tabel_layout.attach(scrolled_window, 0,7, 0, 7)

        #text view
        #self.text_view = Gtk.TextView(editable = False,
        #cursor_visible = False, wrap_mode=2)
        #self.text_buffer = self.text_view.get_buffer()

        #
        #
        #Membuat listview
        ar_id = Gtk.ListStore(int, str, str)

        #
        #
        #Membuat tampilan dari "ar_id"
        tampilan = Gtk.TreeView(model=ar_id)

        #
        #
        #Judul kolom pertama, No. Kolom kedua Indonesia, kolom ketiga Arab
        rend_no = Gtk.CellRendererText(wrap_mode=2, wrap_width=50)
        col_no = Gtk.TreeViewColumn("No", rend_no, text=0)
        tampilan.append_column(col_no)
        #
        #Indo
        rend_indo = Gtk.CellRendererText(wrap_mode=2, wrap_width=500)
        col_indo = Gtk.TreeViewColumn("Indonesia", rend_indo, text=1)
        tampilan.append_column(col_indo)
        #
        #Arab
        rend_arab = Gtk.CellRendererText(wrap_mode=2, wrap_width=100)
        #menjadikan ke kanan
        rend_arab.set_property("xalign", 1.0)
        col_arab = Gtk.TreeViewColumn("Arab", rend_arab, text=2)
        tampilan.append_column(col_arab)

        #
        #
        #Tambahkan ke scrolled window
        scrolled_window.add(tampilan)

        #
        #
        #button and entry connect
        targeem_button.connect('clicked',targeem_pressed.targeem_pressed,
            search_entry, combobox, ar_id )
        search_entry.connect('activate', search_enter.entered,
            combobox, ar_id)


        about = Gtk.Button(stock=Gtk.STOCK_ABOUT, relief=2)
        listkamus = Gtk.Button(stock=Gtk.STOCK_INDEX, relief=2)
        #toolbar_about.insert(about,0)
        #toolbar_about.set_property("xalign", 1.0)
        #toolbar_kamus.insert(listkamus,0)
        tabel_layout.attach(about, 6, 7,7,9)
        tabel_layout.attach(listkamus,0, 1, 7, 9)
        about.connect("clicked", AboutDlg)
        listkamus.connect("clicked", ListKamus )
        #t = Gtk.Tooltip()
        #t.set_tooltip_text(about, "Tentang Targeem")
        #t.set_tip(listkamus, "Buka Kamus Lengkap")
        about.set_tooltip_text("Tentang Targeem")
        listkamus.set_tooltip_text("Buka Kamus")

        self.jendela.show_all()


app = MainGui()
Gtk.main()
