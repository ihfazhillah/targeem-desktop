from gi.repository import Gtk
from moduls.SearchKamus import SearchKamus
import getdir
path = getdir.dimana()
datakamus = SearchKamus(path+'/database/DatabaseMunawirArabIndo.xml')

class ListKamus:

    def __init__(self, widget):
        self.window = Gtk.Window(title="ListKamus")
        self.window.set_default_size(800, 800)
        #self.window.connect('delete-event', Gtk.main_quit)
        #
        #
        #Membuat Model, dengan dua kolom, masing masing isinya string
        #diambil dari listkamusid dan listkamusar
        toko = Gtk.ListStore(str, str)
        listkamus = datakamus.doList()
        for x in range(len(listkamus[0])):
            toko.append([listkamus[0][x]+"\n", listkamus[1][x]+"\n"])
        #
        #
        #Membuat Scrolled Window, dan tambahkan ke window
        sw = Gtk.ScrolledWindow()
        sw.set_hexpand(True)
        sw.set_vexpand(True)
        self.window.add(sw)
        #
        #
        #ini Gtk.TreeView, untuk menampilkan berkas ke aplikasi
        #dengan model=toko, berkas yang kita masukkan tadi di atas
        pohon = Gtk.TreeView(model=toko)
        #
        #
        #Kita kemudian membuat judul kolom pertama, yaitu bahasa indonesia
        #menggunakan CellRendererText, dengan property sbb berikut:
        #1)wrap_mode = 2 (wrap huruf dan kata)
        #2)wrap_width = 650 (seberapa banyak tempat yang akan kita gunakan)
        #kemudian kita membuat kolomnya dengan TreeViewColumn dan kita tambahkan
        #ke pohon dengan append_column()
        rend_indo = Gtk.CellRendererText(wrap_mode=2, wrap_width=550)
        col_indo = Gtk.TreeViewColumn("Indonesia", rend_indo, text=0)
        pohon.append_column(col_indo)
        rend_ar = Gtk.CellRendererText()
        #rend_ar.props.align_set(1)
        #rend_ar.props.alignment(2)
        rend_ar.set_property("xalign", 1.0)
        #rend_ar.set_property("alignment", 2)

        col_ar = Gtk.TreeViewColumn("Arab", rend_ar, text=1)
        pohon.append_column(col_ar)
        sw.add(pohon)
        self.window.show_all()

#app = ListKamus()
#Gtk.main()