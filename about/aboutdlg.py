# -*- coding: utf-8 -*-
from gi.repository import Gtk, GdkPixbuf
import getdir

path = getdir.dimana()
class AboutDlg:

    def __init__(self, widget=None):
        self.adlg = Gtk.AboutDialog()
        self.adlg.set_logo(GdkPixbuf.Pixbuf.new_from_file(path+"/images/bookbsr.png"))
        self.adlg.set_program_name("Targeem")
        self.adlg.set_version("1.0")
        self.adlg.set_authors(["Muhammad Ihfazhillah <mihfazhillah@gmail.com>"
            ])
        self.adlg.set_comments("Aplikasi terjemah kata sederhana Arab"
        "Indo\ndan sebaliknya")
        self.adlg.run()
        self.adlg.destroy()


