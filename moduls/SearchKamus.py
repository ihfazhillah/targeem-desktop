#searchkamus.py
#class pencarian kamus
####################################

#from lxml import  objectify
from moduls import objectify
import re

class SearchKamus:

    def __init__(self, file):
        self.file = file

    def bacaKamus(self):
        self.baca = 'rb'
        with open(self.file, self.baca) as f:
            doc = f.read()
        #baca
        root = objectify.fromstring(doc)
        return root
    #cari dari indo ke arab

    def doList(self):
        """Modul untuk membuat list kamus, arab maupun indo
        return = tuple list indo arab"""
        baca = self.bacaKamus()
        listkamusid = []
        listkamusar = []
        for x in baca.data:
            listkamusid.append(x.attrib['id'])
            listkamusar.append(x.attrib['ar'])
        return listkamusid, listkamusar

    def cariIdAr(self, kata):
        """Modul untuk pencarian dari Indonesia ke Arab,
        keluarannya adalah tuple yang isinya dua list,
        list pertama (berindex 0) adalah indonesia
        list kedua (berindex 1) adalah artian dalam bhs arab"""
        self.kata = kata
        kamus = self.bacaKamus()
        outid = []
        outar = []
        for x in kamus.data:
            if self.kata in x.attrib['id']:
                outar.append(x.attrib['ar'])
                outid.append(x.attrib['id'])
        return  outid, outar

    def cariArId(self, kata):
        """Modul Untuk pencarian dari Arab ke Indonesia
        Keluaran adalah Tuple, index 0 kata arab yang di dapat
        index 1 kata indonesia yang merupakan arti dari itu"""
        self.kata = kata
        kamus = self.bacaKamus()
        outar = []
        outid = []
        for x in kamus.data:
            if self.kata in self.deNoise(x.attrib['ar']):
                outar.append(x.attrib['ar'])
                outid.append(x.attrib['id'])
        return outar, outid


    def cariIndex(self, kata, kms):
        """Modul untuk mencari index kamus
        kata = listkata
        kamus = kamus arab ('ar') atau indo ('id')"""
        kamus = self.bacaKamus()
        listKamus = []
        for x in kamus.data:
            if kms == 'ar':
                listKamus.append(x.attrib['ar'])
            elif kms == 'id':
                listKamus.append(x.attrib['id'])
        #print(listKamus)
        index = []
        for x in kata:
            #print(kata)

            index.append(listKamus.index(x))
        return index



    def deNoise(self, text):
        """Modul Untuk Menghilangkan harokat"""
        noise = re.compile(""" ّ    | # Tashdid
                             َ    | # Fatha
                             ً    | # Tanwin Fath
                             ُ    | # Damma
                             ٌ    | # Tanwin Damm
                             ِ    | # Kasra
                             ٍ    | # Tanwin Kasr
                             ْ    | # Sukun
                             ~    | #Maad
                             ـ     # Tatwil/Kashida
                         """, re.VERBOSE)
        text = re.sub(noise, '', text)
        return text


#cariDiKamus = SearchKamus('/home/sakinah/python/belajarxml/KamusArabIndo/ArabIndo/database/DatabaseMunawirArabIndo.xml')
#print(cariDiKamus.cariIdAr('saya'))
#kamus = cariDiKamus.bacaKamus()
#for x in kamus.data:
#    if 'apakah' in x.attrib['id']:
#        print(x.attrib['ar'])
#keluaran = cariDiKamus.cariIdAr('saya')[0]
#print((keluaran))
#cari = cariDiKamus.cariArId('بكى')
#print(cari)
#idx = cariDiKamus.cariIndex(cari[0], kms='ar')
#print(idx)
#print((cariDiKamus.cariIndex()))
#index = cariDiKamus.cariIndex()
#print(cari[0])
#cariDiKamus.cariIndex()
